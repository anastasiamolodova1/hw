"use strict";

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name(value){
        this._name = value;
    }

    get name() {
            return this._name
        }

    set age(value){
        this._age = value;
    }

    get age() {
        return this._age
    }

    set salary(value){
        this._salary = value;
    }

    get salary() {
        return this._salary
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
     set lang(value){
        this._lang = value;
    }

    get lang() {
        return this._lang
    }
      set salary(value) {
        this._salary= value;
    }
    get salary() {
       return this._salary = value * 3;
        
    }

  
}
const programmer = new Programmer("Jane", 25, 1000, "PYTHON");
const frontendDev = new Programmer("Jon", 30, 2000, "JS");
const backendDev = new Programmer("Bill", 35, 6000, "JAVA");

console.log(programmer);
console.log(frontendDev);
console.log(backendDev);



// "use strict";

// class Employee {
//     constructor(name, age, salary) {
//         this.name = name;
//         this.age = age;
//         this.salary = salary;
//     }
//     set name(name){
//         this._name = name;
//     }

//     get name() {
//             return this._name
//         }

//     set age(age){
//         this._age = age;
//     }

//     get age() {
//         return this._age
//     }

//     set salary(salary){
//         this._salary = salary;
//     }

//     get salary() {
//         return this._salary
//     }
// }


// class Programmer extends Employee {
//     constructor(name, age, salary, lang) {
//         super(name, age, salary);
//         this._lang = lang;
//     }
//      set lang(lang){
//         this._lang = lang;
//     }

//     get lang() {
//         return this._lang
//     }
//     get salary() {
//        return this._salary;
        
//     }
//       set salary(salary) {
//         this._salary= salary * 3;
//     }
    

  
// }
// const programmer = new Programmer("Jane", 25, 1000, "PYTHON");
// const frontendDev = new Programmer("Jon", 30, 2000, "JS");
// const backendDev = new Programmer("Bill", 35, 6000, "JAVA");

// console.log(programmer);
// console.log(frontendDev);
// console.log(backendDev);