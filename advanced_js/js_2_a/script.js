"use strict";

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];







// OLD SOLUTION 

// document.getElementById("root").innerHTML = "<ul id='ul'></ul>"

// books.forEach((e, i) => {
//     try {
//         checkValidity(e, i);
//         document.getElementById('ul').innerHTML += `<li>Book №${i + 1}. Author: ${e.author} Name: ${e.name} Price: ${e.price}</li>`
//     } catch (error) {
//         console.log(error.message);
//     }
// });

// function checkValidity({author, name, price}, i) {
//     if (!author) {
//         throw new Error(`Book №${i + 1}. there is no author here`);
//     }
//     if (!name) {
//         throw new Error(`Book №${i + 1}. there is no name here`);
//     }
//     if (!price) {
//         throw new Error(`Book №${i + 1}. there is no price here`);
//     }
// }

