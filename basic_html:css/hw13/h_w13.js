document.querySelector(".btn").addEventListener("click", () => {
  console.log(localStorage);

  if (localStorage.getItem("site-header") != "black") {
    document.querySelector(".site-header").style.cssText = `background : black;
  opacity: 0.8`;
    document.querySelector(".form-wrapper").style.cssText = `background : black;
  opacity: 0.8; color: white; border: 3px solid white`;
    localStorage.setItem("site-header", "black");

    localStorage.setItem("header-opacity", "0.8");

    localStorage.setItem("form-wrapper", "black");

    localStorage.setItem("form-opacity", "0.8");

    localStorage.setItem("form-text-color", "white");

    localStorage.setItem("form-border", "3px solid white");

    document.querySelector(".header-modal span").style.color = "white";
    localStorage.setItem("header-span", "white");

    document.querySelector(".header-modal a").style.color = "white";
    localStorage.setItem("header-anchor", "white");

    document
      .querySelector(".header-modal .link")
      .addEventListener("mouseover", function () {
        document.querySelector(".header-modal .link").style.color = "#3cb878";
        console.log("over");
      });

    document
      .querySelector(".header-modal .link")
      .addEventListener("mouseout", function () {
        document.querySelector(".header-modal .link").style.color = "white";
        console.log("over");
      });
  } else {
    localStorage.setItem("site-header", "");
    document.querySelector(".site-header").style.cssText = `background : '';
     opacity: ''`;

    document.querySelector(".form-wrapper").style.cssText = `background : '';
      opacity: ''; 
      color: 'white'; 
      border: ''`;

    localStorage.setItem("header-opacity", "");

    localStorage.setItem("form-wrapper", "");

    localStorage.setItem("form-opacity", "");

    localStorage.setItem("form-text-color", "");

    localStorage.setItem("form-border", "");

    document.querySelector(".header-modal span").style.color = "black";
    localStorage.setItem("header-span", "");

    document.querySelector(".header-modal a").style.color = "black";
    localStorage.setItem("header-anchor", "");

    document
      .querySelector(".header-modal .link")
      .addEventListener("mouseover", function () {
        document.querySelector(".header-modal .link").style.color = "#3cb878";
        console.log("over");
      });

    document
      .querySelector(".header-modal .link")
      .addEventListener("mouseout", function () {
        document.querySelector(".header-modal .link").style.color = "black";
        console.log("over");
      });
  }
});

document.addEventListener("DOMContentLoaded", function f1() {
  document.querySelector(".site-header").style.background =
    localStorage.getItem("site-header");
  document.querySelector(".form-wrapper").style.opacity =
    localStorage.getItem("form-opacity");

  document.querySelector(".site-header").style.opacity =
    localStorage.getItem("header-opacity");
  document.querySelector(".form-wrapper").style.background =
    localStorage.getItem("form-wrapper");

  document.querySelector(".form-wrapper").style.border =
    localStorage.getItem("form-border");

  document.querySelector(".header-modal span").style.color =
    localStorage.getItem("header-span");

  document.querySelector(".header-modal a").style.color =
    localStorage.getItem("header-anchor");

  document.querySelector(".form-wrapper").style.color =
    localStorage.getItem("form-text-color");

  document
    .querySelector(".header-modal .link")
    .addEventListener("mouseover", function () {
      document.querySelector(".header-modal .link").style.color = "#3cb878";
      console.log("over");
    });
  document
    .querySelector(".header-modal .link")
    .addEventListener("mouseout", function () {
      document.querySelector(".header-modal a").style.color =
        localStorage.getItem("header-anchor");
    });
});
