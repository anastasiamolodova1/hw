const array = [ 
  'Blue',
  'Red',
  'White',
  'Green',
  'Black',
  'Orange'];

function makeList(arr,parent='body' ) {
    const ul = document.createElement('ul');
    const p = document.createElement("p");
    document.querySelector(parent).append(ul);
    document.querySelector(parent).prepend(p);

    let listItem = arr.map((color) =>  {
        let li = document.createElement("li");
        li.innerText = color;
        ul.append(li);
    })
}

makeList(array);



 
 