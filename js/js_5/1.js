
function createNewUser(){
    let newUser = {
        getLogin: function() {
            return console.log(this.firstName[0].toLowerCase() + this.secondName.toLowerCase()) ;
        },
        getAge: function () {
            let now = new Date();
            let currentYear = now.getFullYear();

            let inputDate = +this.birthday.substring(0, 2);
            let inputMonth = +this.birthday.substring(3, 5);
            let inputYear = +this.birthday.substring(6, 10);

            let birthDate = new Date(inputYear, inputMonth-1, inputDate);
            let birthYear = birthDate.getFullYear();
            let age = currentYear - birthYear;
            if (now < new Date(birthDate.setFullYear(currentYear))) {
                age = age - 1;
            }
            return console.log(age);
        },
        getPassword: function () {

            return console.log(this.firstName[0].toUpperCase() + this.secondName.toLowerCase() + this.birthday.substring(6,10));
        }
    };
    newUser.firstName = prompt("Write your first name");
    newUser.secondName = prompt("Write your second name");
    newUser.birthday = prompt("Write your birthday dd.mm.yyyy ");
    newUser.getAge();
    newUser.getPassword();

    return newUser;
}
createNewUser();


// еще вариант решения этой задачки

// class CreateNewUser {
// 	constructor() {
// 		this.firstName = prompt("Write your first name");
// 		this.lastName = prompt("Write your second name");
// 		this.birthday = prompt("Write your birthday", "text in format dd.mm.yyyy");
// 	};

// 	getLogin() {
// 		return `${this.firstName[0].toLowerCase()}.${this.lastName.toLowerCase()}`;
// 	};

// 	getAge() {
// 		const today = new Date();
// 		const userBirthday = Date.parse(`${this.birthday.slice(6)}-${this.birthday.slice(3, 5)}-${this.birthday.slice(0, 2)}`);
// 		const age = ((today - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
// 		if (age < today) {
// 			return `Вам ${age - 1} лет`;
// 		} else {
// 			return `Вам ${age} лет`;
// 		}
// 	};

// 	getPassword() {
// 		return `${this.firstName[0].toUpperCase()}${this.lastName.toLocaleLowerCase()}${this.birthday.slice(-4)}`
// 	};
// }

// const newUser = new CreateNewUser();
// console.log(newUser.getAge());
// console.log(newUser.getPassword());
