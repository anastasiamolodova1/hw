let fNum;
let sNum;
let operation;

const checkIfNumber = (num) => Number.isNaN(Number.parseFloat(num));
const checkIfOperation = (op) => op === "+" || op === "-" || op === "*" || op === "/";

do{
  fNum = prompt("enter the first number", fNum);
  if (fNum === null) break;
  sNum = prompt("enter the second number", sNum);
  if (sNum === null) break;
  operation = prompt("choose math operation:  +, -, /, *", operation);
  } 
  while (checkIfNumber(fNum) || checkIfNumber(sNum) || !checkIfOperation(operation));

function calc(a, b, opr){ 
  if(opr === '+'){
    return add(a,b)
  } else if(opr === '-') {
  return deduct(a,b);
  }else if(opr === '*') {
    return multiply(a,b);
  } else if(opr === '/'){
    return divide(a,b)

  }
  }
  console.log(`${fNum} ${operation} ${sNum} = ${calc(fNum, sNum, operation)}`);
function deduct(a,b){
  return Number(a)- Number(b);
  }
function add(a,b){
  return Number(a) + Number(b) ;
  }
function multiply(a,b){
    return Number(a)*Number(b);
  }
function divide(a,b){
  return Number(a)/Number(b);
  }
//  или вместо if вариант со switch
   
// function calc (a,b,opr) {
//     switch (opr) {
//         case '+':
//             return (Number(a)+Number(b)); 
//         case '-':
//             return (Number(a)-Number(b)); 
//         case '*':
//             return (Number(a)*Number(b)); 
//         case '/':
//             return (Number(a)/Number(b)); 
//     }
// }

//   console.log(`${fNum} ${operation} ${sNum} = ${calc(fNum, sNum, operation)}`);
