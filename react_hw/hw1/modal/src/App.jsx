import { useState } from 'react'
import ModalImage from './components/ModalImage/ModalImage'
import ModalText from './components/ModalText/ModalText'

import './styles/styles.scss'

function App() {
  const [modalImg, setModalImg] = useState(false)
  const [modalText, setModalText] = useState(false)

  const handleModalImage = () => {
    setModalImg(!modalImg)
  }

  const handleModalText = () => {
    setModalText(!modalText)
  }

  return (
    <>
      <button type='button' onClick={handleModalImage}>ModalImage</button>
      <button type='button' onClick={handleModalText}>ModalText</button>

      <ModalImage isOpen={modalImg} onClick={handleModalImage} />
      <ModalText isOpen={modalText} onClick={handleModalText} />
    </>
  )
}

export default App
