import React from "react";

const ModalBox = ({children, className}) => {
    return (
        <div className={`modal ${className}`}>
                <div className="modal-box">{children}</div>
            </div>
    );
};

export default ModalBox;