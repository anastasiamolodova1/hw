import React from "react";
import PropTypes from 'prop-types'

import './ModalBase.scss'

const ModalWrapper = ({isOpen = false, children}) => {
    // hendleClickOutSide
    return(
       <>
            {
                isOpen && (<div className="modal-wrapper">{children}</div>)    
            }
       </> 
    )
};

export default ModalWrapper;
