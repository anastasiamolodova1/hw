import React from "react";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalBox from "../Modal/ModalBox";
import ModalHeader from "../Modal/ModalHeader";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import "./ModalImage.scss";
import ModalClose from "../Modal/ModalClose";

const ModalImage = ({ isOpen, onClick }) => {
  return (
    <ModalWrapper isOpen={isOpen}>
      <ModalBox className="modal-image">
        <ModalClose onClick={onClick} />
        <ModalHeader>
          <img src="" alt="image" />
        </ModalHeader>
        <ModalBody>
          <h3>Product Delete!</h3>
          <p>
            By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.
          </p>
        </ModalBody>
        <ModalFooter>
          <button className="btn-cancel" type="button">
            NO, CANCEL
          </button>
          <button className="btn-delete" type="button" onClick={onClick}>
            YES, DELETE
          </button>
        </ModalFooter>
      </ModalBox>
    </ModalWrapper>
  );
};

export default ModalImage;
