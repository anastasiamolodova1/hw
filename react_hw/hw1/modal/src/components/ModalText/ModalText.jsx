import React from "react";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalBox from "../Modal/ModalBox";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import ModalClose from "../Modal/ModalClose";
import "./ModalText.scss";

const ModalText = ({ isOpen, onClick }) => {
  return (
    <ModalWrapper isOpen={isOpen}>
      <ModalBox className="modal-text">
        <ModalClose onClick={onClick} />
        <ModalBody>
          <h3>Add Product “NAME”</h3>
          <p>Description for you product</p>
        </ModalBody>
        <ModalFooter>
          <button type="button">ADD TO FAVORITE</button>
        </ModalFooter>
      </ModalBox>
    </ModalWrapper>
  );
};

export default ModalText;
