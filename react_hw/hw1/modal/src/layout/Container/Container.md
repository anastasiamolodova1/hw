### Html структура

```html
<div class="g-container">
    {children}
</div>
```


#### Props

```js
const props = {
	children: "any",
};
```

#### Imports

```js
import React from "react";
import './Container.scss'
```
